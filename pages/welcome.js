import React from "react";

import Header from "./components/Header";
import Footer from "./components/Footer";
import Welcome from "./components/Welcome";

function welcome() {
  return (
    <>
      <Header />
      <Welcome />
      <Footer />
    </>
  );
}

export default welcome;
