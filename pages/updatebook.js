import React from "react";

import Header from "./components/Header";
import Footer from "./components/Footer";
import UpdateBook from "./components/UpdateBook";

export default function updateBook() {
  return (
    <>
      <Header />
      <UpdateBook />
      <Footer />
    </>
  );
}