import React from "react";

import Header from "./components/Header";
import Footer from "./components/Footer";
import AddBook from "./components/AddBook";

function addbooks() {
  return (
    <>
      <Header />
      <AddBook />
      <Footer />
    </>
  );
}

export default addbooks;
