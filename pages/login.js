import React from "react";

import Header from "./components/Header";
import Login from "./components/Login";
import Footer from "./components/Footer";

function login() {
  return (
    <>
      <Header />
      <Login />
      <Footer />
    </>
  );
}

export default login;
