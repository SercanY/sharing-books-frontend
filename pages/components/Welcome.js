import React, { useEffect, useState } from "react";
import Logout from "./Logout";
import MyBooksButton from "./MyBooksButton";

function Welcome() {
  const [name, setName] = useState("");

  useEffect(() => {
    const email = window?.localStorage.getItem("email");
    const fdEmail = new FormData();
    fdEmail.append("email", email);

    findMe(fdEmail);
  });

  const findMe = (fdEmail) => {
    fetch("http://localhost:8080/api/v1/me", {
      method: "post",
      body: fdEmail,
    })
      .then((res) => res.json())
      .then((json) => {
        window?.localStorage.setItem(
          "Name-Surname",
          json.body.name + " " + json.body.surname
        );
        setName(json.body.name);
      });
  };

  return (
    <div className="container h-100">
      <div
        className="row h-100 justify-content-center"
        style={{ minHeight: "100vh" }}
      >
        <div className="col-10 col-md-8 col-lg-6">
          <h1>WELCOME {name}</h1>
          <MyBooksButton />
          <Logout />
        </div>
      </div>
    </div>
  );
}

export default Welcome;
