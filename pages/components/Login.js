import React, { useState } from "react";
import { useRouter } from "next/router";
import Link from "next/link";

export default function Login() {
  const [error, setError] = useState("");

  const [details, setDetails] = useState({ email: "", password: "" });

  const router = useRouter();

  const submitHandler = (e) => {
    e.preventDefault();

    Login(details);
  };

  const Login = (details) => {
    fetch("http://localhost:8080/api/v1/login", {
      method: "post",
      body: JSON.stringify(details),
      headers: { "Content-Type": "application/json" },
    }).then((res) => {
      if (res.status === 200) {
        router.push("/welcome");
        localStorage.setItem('email', details.email);
      } else if (res.status === 401) {
        setError("!!!BAD CREDENTIALS!!!");
      } else {
        setError("Something wrong!");
      }
    });
  };

  return (
    <div className="container h-100">
      <div className="row h-100 justify-content-center" style={{minHeight: "100vh"}}>
        <div className="col-10 col-md-8 col-lg-6">
          <form className="px-" onSubmit={submitHandler}>
            <div>
              <h2>Login</h2>
              <div className="form group">
                {error != "" ? <div className="text-error">{error}</div> : ""}
                <label htmlFor="exampleInputEmail1" htmlFor="email">
                  Email:
                </label>
                <input
                  type="email"
                  className="form-control"
                  name="email"
                  id="exampleInputEmail1"
                  aria-describedby="emailHelp"
                  placeholder="Enter email"
                  onChange={(e) =>
                    setDetails({ ...details, email: e.target.value })
                  }
                  value={details.email}
                />
              </div>
              <div className="form-group">
                <label htmlFor="exampleInputPassword1" htmlFor="password">
                  Password:
                </label>
                <input
                  type="password"
                  className="form-control"
                  name="password"
                  id="exampleInputPassword1"
                  placeholder="Password"
                  onChange={(e) =>
                    setDetails({ ...details, password: e.target.value })
                  }
                  value={details.password}
                />
              </div>
              <div>
                <input type="submit" className="btn btn-primary" value="LOGIN" />
              </div>
              <div>
                Haven't you sign up? Go to{" "}
                <Link href="/registration">
                  <a>Register</a>
                </Link>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
}
