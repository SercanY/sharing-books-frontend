import React from "react";
import Link from "next/link";

function Main() {
  return (
    <div className="container">
      <div className="row">
        <div className="col text-center" style={{minHeight: "100vh"}}>
         <h2>Do you have an account?</h2> 
          <Link href="/login">
            <button className="btn btn-large btn-primary"><h3>Login</h3></button>
          </Link>
        </div>
        <div className="col text-center">
        <h2>Are you new user?</h2> 
          <Link href="/registration">
            <button className="btn btn-large btn-success"><h3>Register</h3></button>
          </Link>
        </div>
      </div>
    </div>
  );
}

export default Main;
