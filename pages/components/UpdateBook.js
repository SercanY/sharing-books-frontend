import { Router, useRouter } from "next/router";
import React, { useEffect, useState } from "react";

let count = 0;

export default function UpdateBook() {
  const router = useRouter();

  const [error, setError] = useState("");

  const [details, setDetails] = useState({
    bid: "",
    name: "",
    category: "",
    author: "",
    publisher: "",
    dop: "",
    currentOwner: "",
    tradeType: "",
  });

  useEffect(() => {
    if (count === 0) {
      findBook(window?.localStorage.getItem("bid"));
    }
  }, []);

  const findBook = (bid) => {
    const fdBid = new FormData();
    fdBid.append("bid", bid);

    fetch("http://localhost:8080/api/v1/book/getBook", {
      method: "post",
      body: fdBid,
    })
      .then((res) => res.json())
      .then((json) => {
        setDetails({
          name: json.body.name,
          category: json.body.category,
          author: json.body.author,
          publisher: json.body.publisher,
          dop: json.body.dop,
          currentOwner: json.body.currentOwner,
          tradeType: json.body.tradeType,
        });
      });
  };

  const submitHandler = (details) => {
    console.log(details);
    debugger;
    /* fetch("http://localhost:8080/api/v1/book/updateBook", {
      method: "put",
      body: details,
    })
      .then((res) => res.json())
      .then((json) => {
        console.log(json.message);
      });*/
  };

  const CleanUp = () => {
    window?.localStorage.removeItem("bid");
    router.push("/mybooks");
  };

  return (
    <div className="container h-100">
      <div
        className="row h-100 justify-content-center"
        style={{ minHeight: "100vh" }}
      >
        <div className="col-10 col-md-8 col-lg-6">
          <form
            onSubmit={() => {
              submitHandler(details)
            }}
          >
            <h1> Update Book</h1>
            {error != "" ? <div className="text-error">{error}</div> : ""}
            <div className="form-row">
              <label>Name:</label>
              <input
                name="name"
                type="text"
                className="form-control"
                placeholder="Name"
                onChange={(e) =>
                  setDetails({ ...details, name: e.target.value })
                }
                value={details.name}
              />
            </div>
            <div className="form-row">
              <div>
                <label>Category:</label>
              </div>
              <select
                className="custom-select px-2"
                id="inputCategory"
                onChange={(e) =>
                  setDetails({ ...details, category: e.target.value })
                }
                value={details.category}
              >
                <option defaultValue>Choose Type</option>
                <option value="Fantastic">Fantastic</option>
                <option value="Science-fiction">Science-fiction</option>
                <option value="Scary">Scary</option>
                <option value="Adventure">Adventure</option>
                <option value="Love">Love</option>
                <option value="Psychological">Psychological</option>
                <option value="Classic">Classic</option>
              </select>
            </div>
            <div className="form-row">
              <label>Author:</label>
              <input
                name="Author"
                type="text"
                className="form-control"
                placeholder="Author"
                onChange={(e) =>
                  setDetails({ ...details, author: e.target.value })
                }
                value={details.author}
              />
            </div>

            <div className="form-row">
              <label>Publisher:</label>
              <input
                name="publisher"
                type="text"
                className="form-control"
                placeholder="Publisher"
                onChange={(e) =>
                  setDetails({ ...details, publisher: e.target.value })
                }
                value={details.publisher}
              />
            </div>
            <div className="form-row">
              <label>Date of Publish:</label>
              <input
                name="dop"
                type="text"
                className="form-control"
                placeholder="mm/yyyy"
                onChange={(e) =>
                  setDetails({ ...details, dop: e.target.value })
                }
                value={details.dop}
              />
            </div>
            <div className="form-row">
              <div>
                <label>Trade Type:</label>
              </div>
              <select
                className="custom-select px-2"
                id="inputTradeType"
                onChange={(e) =>
                  setDetails({ ...details, tradeType: e.target.value })
                }
                value={details.tradeType}
              >
                <option defaultValue>Choose Type</option>
                <option value="OWNED">OWNED</option>
                <option value="WISHLIST">WISHLIST</option>
                <option value="TRADABLE">TRADABLE</option>
              </select>
            </div>
            <div className="form-group row">
              <div className="col-md-6 text-center p-3">
                <button
                  type="submit"
                  id="btnUpdate"
                  className="btn btn-primary"
                >
                  UPDATE
                </button>
              </div>
              <div className="col-md-6 text-center p-3">
                <button
                  onClick={() => {
                    CleanUp();
                  }}
                  type="button"
                  id="btnCancel"
                  className="btn btn-dark"
                >
                  <a href="/mybooks">CANCEL</a>
                </button>
              </div>
              <h6>
                !!!After update, you will be redirect to "My Books" page!!!
              </h6>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
}
