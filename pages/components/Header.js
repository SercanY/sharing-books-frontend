import React from "react";
import { useRouter } from "next/router";

function Header() {

  const router = useRouter();

  function goToWelcome(){
    if(window?.localStorage.getItem("email") === null){
      router.push("/");
    }
    else{
      router.push("/welcome");
    }
  }

  return (
    <div>
      <img src="/header.jpg" alt="Header Logo" onClick={goToWelcome} style={{cursor: "pointer", width: "100%", height: "10%"}}/>
    </div>
    
  );
}

export default Header;
