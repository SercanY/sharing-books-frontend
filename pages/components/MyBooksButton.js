import Router, { useRouter } from "next/router";

export default function MyBooksButton() {
  const router = useRouter();

  function myBooks() {
    router.push("/mybooks");
  }

  return (
    <div>
      <button className="btn btn-warning" onClick={myBooks}>
        My Books
      </button>
    </div>
  );
}
