import { useRouter } from "next/router";
import React, { useState } from "react";

export default function AddBook() {

  const [error, setError] = useState("");

  const [details, setDetails] = useState({
    name: "",
    category: "",
    author: "",
    publisher: "",
    dop: "",
    currentOwner:"",
    tradeType: "",
  });

  const router = useRouter();

  const submitHandler = (e) => {
    e.preventDefault();
    details.currentOwner = window?.localStorage.getItem('email');

    var book = JSON.stringify(details);
    SaveBook(book);
    setDetails({name:"",category:"",author:"",publisher:"",dop:"",currentOwner:"",tradeType:""});
  };

  const SaveBook = (book) => {
    fetch("http://localhost:8080/api/v1/book/saveBook", {
      method: "post",
      body: book,
      headers: { "Content-Type": "application/json" },
  })
      .then((res) => res.json())
      .then((json) => {
        setError(json.message);
      }); 
  };

  return (
    <div className="container h-100">
      <div className="row h-100 justify-content-center" style={{minHeight: "100vh"}}>
        <div className="col-10 col-md-8 col-lg-6">
          <form onSubmit={submitHandler}>
            <h1> Add Book</h1>
            {error != "" ? <div className="text-error">{error}</div> : ""}
            <div className="form-row">
              <label>Name:</label>
              <input
                name="name"
                type="text"
                className="form-control"
                placeholder="Name"
                onChange={(e) =>
                  setDetails({ ...details, name: e.target.value })
                }
                value={details.name}
              />
            </div>
            <div className="form-row">
              <div>
                <label>Category:</label>
              </div>
              <select className="custom-select px-2" id="inputCategory" onChange={(e) =>
                  setDetails({ ...details, category: e.target.value })
                }
                value={details.category}>
                <option defaultValue>Choose Type</option>
                <option value="Fantastic">Fantastic</option>
                <option value="Science-fiction">Science-fiction</option>
                <option value="Scary">Scary</option>
                <option value="Adventure">Adventure</option>
                <option value="Love">Love</option>
                <option value="Psychological">Psychological</option>
                <option value="Classic">Classic</option>
              </select>
            </div>
            <div className="form-row">
              <label>Author:</label>
              <input
                name="Author"
                type="text"
                className="form-control"
                placeholder="Author"
                onChange={(e) =>
                    setDetails({ ...details, author: e.target.value })
                  }
                  value={details.author}
              />
            </div>

            <div className="form-row">
              <label>Publisher:</label>
              <input
                name="publisher"
                type="text"
                className="form-control"
                placeholder="Publisher"
                onChange={(e) =>
                    setDetails({ ...details, publisher: e.target.value })
                  }
                  value={details.publisher}
              />
            </div>
            <div className="form-row">
              <label>Date of Publish:</label>
              <input
                name="dop"
                type="text"
                className="form-control"
                placeholder="mm/yyyy"
                onChange={(e) =>
                    setDetails({ ...details, dop: e.target.value })
                  }
                  value={details.dop}
              />
            </div>
            <div className="form-row">
              <div>
                <label>Trade Type:</label>
              </div>
              <select className="custom-select px-2" id="inputTradeType" onChange={(e) =>
                  setDetails({ ...details, tradeType: e.target.value })
                }
                value={details.tradeType}>
                <option defaultValue>Choose Type</option>
                <option value="OWNED">OWNED</option>
                <option value="WISHLIST">WISHLIST</option>
                <option value="TRADABLE">TRADABLE</option>
              </select>
            </div>
            <div className="form-group row">
              <div className="col-md-6 text-center p-3">
                <button type="submit" id="btnSubmit" className="btn btn-primary">
                  SAVE
                </button>
              </div>
              <div className="col-md-6 text-center p-3">
                <button type="button" id="btnCancel" className="btn btn-dark">
                  <a href="/mybooks">CANCEL</a>
                </button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
}
