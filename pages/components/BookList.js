import React, { useEffect, useState } from "react";
import Link from "next/link";
import { useRouter } from "next/router";

let count = 0;

function BookList() {
  const [message, setMessage] = useState("");
  const [bookList, setBookList] = useState([]);

  const router = new useRouter();

  useEffect(() => {
    const email = window?.localStorage.getItem("email");
    const fdEmail = new FormData();
    fdEmail.append("email", email);

    if (count === 0) {
      findMyBooks(fdEmail);
      count++;
    }
  }, [bookList]);

  const findMyBooks = async (fdEmail) => {
    fetch("http://localhost:8080/api/v1/book/myBooks", {
      method: "post",
      body: fdEmail,
    })
      .then((res) => res.json())
      .then((json) => {
        setBookList(json.body);
      });
  };

  const handleDelete = (bid) => {
    const fdBid = new FormData();
    fdBid.append("bid", bid);

    fetch("http://localhost:8080/api/v1/book/delete", {
      method: "post",
      body: fdBid,
    })
      .then((res) => res.json())
      .then((json) => {
        setMessage(json.message);
      });
  };

  const deleteRow = (bid) => {
    let copy = [...bookList];
    copy.splice(bid, 1);
    setBookList(copy);
  };

  const handleSortByName = async (key) => {
    let a = [...bookList];
    a = bookList.sort((a, b) => a.name.localeCompare(b.name));
    await setBookList([]);
    setBookList(a);
  };

  const handleUpdate = async (bid) => {
    window?.localStorage.setItem("bid", bid);
    router.push("/updatebook");
  };

  return (
    <div className="container h-100" style={{ minHeight: "100vh" }}>
      <div className="row h-100 justify-content-center">
        <div className="col-10 col-md-8 col-lg-6"></div>
        <div>
          <div className="container my-3" style={{ backgroundColor: "bisque" }}>
            <div
              className="col-md-12 text-center"
              style={{ backgroundColor: "bisque" }}
            >
              <Link href="/addbooks" class="btn btn-outline-dark" passHref>
                <button type="button" className="btn btn-primary">
                  Add Book
                </button>
              </Link>
            </div>
          </div>
          {message != "" ? <div className="text-error">{message}</div> : ""}
          <table className="table table-stripped">
            <thead className="thead-dark">
              <tr>
                <th
                  scope="col"
                  onClick={() => {
                    handleSortByName();
                  }}
                >
                  Name
                </th>
                <th scope="col">Category</th>
                <th scope="col">Author</th>
                <th scope="col">Publisher</th>
                <th scope="col">Date of Publish</th>
                <th scope="col">Trade Type</th>
                <th scope="col">Delete?</th>
                <th scope="col">Update</th>
                <th scope="col">See the Sellers</th>
              </tr>
            </thead>
            <tbody>
              {Object.keys(bookList).map(function (bid) {
                return (
                  <tr key={bid}>
                    <td>{bookList[bid].name}</td>
                    <td>{bookList[bid].category}</td>
                    <td>{bookList[bid].author}</td>
                    <td>{bookList[bid].publisher}</td>
                    <td>{bookList[bid].dop}</td>
                    <td>{bookList[bid].tradeType}</td>
                    <td>
                      <button
                        className="btn btn-outline-dark"
                        onClick={() => {
                          handleDelete(bookList[bid].bid), deleteRow(bid);
                        }}
                      >
                        DELETE
                      </button>
                    </td>
                    <td>
                      <button
                        className="btn btn-outline-dark"
                        onClick={() => {
                          handleUpdate(bookList[bid].bid);
                        }}
                      >
                        UPDATE
                      </button>
                    </td>
                    <td>
                      <button className="btn btn-outline-dark">Go</button>
                    </td>
                  </tr>
                );
              })}
            </tbody>
          </table>
        </div>
      </div>
    </div>
  );
}

export default BookList;
