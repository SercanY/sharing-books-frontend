import React, { useState } from "react";
import { useForm } from "react-hook-form";
import Link from "next/link";
import { useRouter } from "next/router";

function Register() {
  const [error, setError] = useState("");
  const router = useRouter();

  const onSubmit = (data) => {
    fetch("http://localhost:8080/api/v1/registration", {
      method: "post",
      body: JSON.stringify({
        name: data.name,
        surname: data.surname,
        email: data.email,
        password: data.password,
      }),
      headers: { "Content-Type": "application/json" },
    })
      .then((res) => res.json())
      .then((json) => {
        setError(json.message);
        router.push("/login");
      });
  };

  const { register, handleSubmit, errors } = useForm();

  return (
    <div className="container h-100">
      <div className="row h-100 justify-content-center" style={{minHeight: "100vh"}}>
        <div className="col-10 col-md-8 col-lg-6">
          <form onSubmit={handleSubmit(onSubmit)}>
            <h1> Sign Up</h1>

            {error != "" ? <div>{error}</div> : ""}

            <div className="form-row">
              <div className="col">
                <label>Name:</label>
                <input
                  name="name"
                  type="text"
                  className="form-control"
                  placeholder="Name"
                  ref={register({ required: true, minLength: 3 })}
                />
                {errors.name && <p>Minimum 3 length name is required</p>}
              </div>
              <div className="col">
                <label>Surname:</label>
                <input
                  name="surname"
                  type="text"
                  className="form-control"
                  placeholder="Surname"
                  ref={register({ required: true, minLength: 3 })}
                />
                {errors.surname && <p>Minimum 3 length surname is required</p>}
              </div>
            </div>
            <div className="form-group">
              <label htmlFor="inputEmail1">Email:</label>
              <input
                type="email"
                className="form-control"
                id="inputEmail1"
                placeholder="Email"
                name="email"
                ref={register({ required: true })}
              />
              {errors.email && <p>Email is required</p>}
            </div>

            <div className="form-group">
              <label htmlFor="inputPassword2">Password:</label>
              <input
                type="password"
                className="form-control"
                id="inputPassword2"
                placeholder="Password"
                name="password"
                ref={register({ required: true, minLength: 6 })}
              />
              {errors.password && <p>Minimum 6 length password is required</p>}
            </div>
            <br></br>
            <div>
              <input type="submit" className="btn btn-primary" />
              <br></br>
              <h6>!!!After submitting, you will be redirect to login page!!!</h6>
              <br></br>
              <div>
                Already signed-up? Go to{" "}
                <Link href="/login">
                  <a>Login</a>
                </Link>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
}

export default Register;
