import React from "react";

function Footer() {
  return (
    <p className="copyright m-2 pl-2 static-bottom">
      &copy; Sharing Books tutorial by{" "}
      <a href="https://www.linkedin.com/in/sercan-yilmaz-039a141b1/">
        Sercan YILMAZ
      </a>{" "}
      and
      <a href="https://www.linkedin.com/in/mertkose/"> Mert Köse</a>
    </p>
  );
}

export default Footer;
