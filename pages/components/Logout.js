import Link from 'next/link';
import react from 'react';
import Router, { useRouter } from 'next/router';

export default function Logout(){

    const router = useRouter();

    function cleaning(){
        window?.localStorage.clear();
        router.push("/");
    }

    return(
        <div>
        <button className="btn btn-outline-dark" onClick={cleaning}>Logout</button>
      </div>
    )
}