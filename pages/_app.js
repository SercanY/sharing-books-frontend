import '../styles/globals.css'
import 'bootstrap/dist/css/bootstrap.min.css';
import '../styles/myCss/main.css';


function MyApp({ Component, pageProps }) {
  return <Component {...pageProps} />
}

export default MyApp
